<?php
/*
Template Name: Projects Page
*/
get_header();
global $ids;
?>

<div id="content" class="span8" role="main">

	<header class="entry-header">
		<h1 class="entry-title"><?php single_cat_title( '', true ); ?></h1>
		<?php edit_post_link(__('Edit This Page', 'largo'), '<h5 class="byline"><span class="edit-link">', '</span></h5>'); ?>
	</header><!-- .entry-header -->

	<div class="content-top">
		<?php
			$tax_args = array(
				'orderby' 	=> 'id',
				'taxonomy' 	=> 'series',
				'name'		=> 'term_id',
				'id'		=> 'term_id'
		    );

			$series = get_categories($tax_args);
			foreach ( $series as $series ) {

				$args = array(
					'tax_query' => array(
						array(
							'taxonomy' 	=> 'series',
							'field' 	=> 'slug',
							'terms' 	=> $series->slug
						)
					),
					'showposts'		=> 2
				);
				$query = new WP_Query($args);
				$count = 1;
				if ( $query->have_posts() ) :
					while ( $query->have_posts() ) : $query->the_post(); $ids[] = get_the_ID();
						if ($count == 1) {
			?>

				<div class="item row-fluid">
					<div class="photo span3">
						<a href="<?php echo get_term_link( $series, $series->taxonomy ); ?>"><?php the_post_thumbnail(); ?></a>
					</div>
					<div class="item-content span9">
						<h3><a href="<?php echo get_term_link( $series, $series->taxonomy ); ?>"><?php echo $series->name; ?></a></h3>
						<?php if ($series->category_description) echo '<p>' . $series->category_description . '</p>'; ?>
						<h5 class="recent">Latest Updates:</h5>
						<ul>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

			<?php
						} else {
			?>

							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
						</ul>
					</div>
				</div>
			<?php
						}
						$count++;
						endwhile;
					endif;
				}
			?>
	</div>

	<h3 class="recent-posts clearfix">
		<?php printf(__('More Recent Investigations<a class="rss-link" href="%1$s"><i class="icon-rss"></i></a>', 'largo'), get_category_feed_link( get_queried_object_id() ) ); ?>
	</h3>

	<div class="stories">
		<?php
			$args = array(
				'cat'				=> 31,
				'post_per_page'		=> 10,
				'post__not_in'		=> $ids,
				'paged' => $paged
			);

			$query = new WP_Query( $args );
			if ( $query->have_posts() ) :
				while ( $query->have_posts() ) : $query->the_post();
					get_template_part( 'content', 'archive' );
				endwhile;
			endif;

			largo_content_nav( 'nav-below' );
		?>
	</div>

</div><!-- #content -->

<?php get_footer(); ?>