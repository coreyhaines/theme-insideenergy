<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 */
?>
	<div class="super-footer">
		<h5> Inside Energy is a collaborative journalism initiative of partners across the US and supported by the Corporation for Public Broadcasting</h5>
		<div class="row-fluid">
			<?php $dir = get_stylesheet_directory_uri(); ?>
			<div class="span2"><a href="http://cpb.org/"><img src="<?php echo $dir; ?>/img/CPB_200.jpg" /></a></div>
			<div class="span2"><a href="http://www.cpt12.org/"><img src="<?php echo $dir; ?>/img/CPT_200.jpg" /></a></div>
			<div class="span2"><a href="http://kunc.org/"><img src="<?php echo $dir; ?>/img/KUNC_200.jpg" /></a></div>
			<div class="span2"><a href="http://kuvo.org/"><img src="<?php echo $dir; ?>/img/KUVO_200.jpg" /></a></div>
			<div class="span2"><a href="http://www.prairiepublic.org/"><img src="<?php echo $dir; ?>/img/Prarie_200.jpg" /></a></div>
			<div class="span2"><a href="http://www.rmpbs.org/home/"><img src="<?php echo $dir; ?>/img/RMPBS_200.jpg" /></a></div>
			<div class="span2"><a href="http://wyomingpublicmedia.org/"><img src="<?php echo $dir; ?>/img/WPM_200.jpg" /></a></div>
			<div class="span2"><a href="http://www.wyomingpbs.org/"><img src="<?php echo $dir; ?>/img/WyPBS_200.jpg" /></a></div>
		</div>
	</div>

	</div> <!-- #main -->

</div><!-- #page -->

<div class="footer-bg clearfix">
	<footer id="site-footer">
		<div id="supplementary" class="row-fluid">
			<?php get_template_part( 'footer-part', 'widget-area' ); ?>
		</div>
		<div id="boilerplate" class="row-fluid clearfix">
			<p><?php largo_copyright_message(); ?></p>
			<?php wp_nav_menu( array( 'theme_location' => 'footer-bottom', 'container' => false, 'depth' => 1  ) ); ?>
			<div class="footer-bottom clearfix">
				<!-- If you enjoy this theme and use it on a production site we would appreciate it if you would leave the credit in place. Thanks :) -->
				<p class="footer-credit"><?php _e('This site built with <a href="http://largoproject.org">Project Largo</a> from the <a href="http://investigativenewsnetwork.org">Investigative News Network</a> and proudly powered by <a href="http://wordpress.org" rel="nofollow">WordPress</a>.', 'largo'); ?></p>
				<p class="back-to-top"><a href="#top"><?php _e('Back to top &uarr;', 'largo'); ?></a></p>
			</div>
		</div><!-- /#boilerplate -->
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>